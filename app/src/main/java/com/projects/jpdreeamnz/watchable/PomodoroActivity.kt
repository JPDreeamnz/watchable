package com.projects.jpdreeamnz.watchable

import android.os.Bundle
import android.support.wearable.activity.WearableActivity

class PomodoroActivity : WearableActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pomodoro)

        // Enables Always-on
        setAmbientEnabled()
    }
}
